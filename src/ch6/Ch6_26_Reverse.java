package ch6;

import java.util.Scanner;

public class Ch6_26_Reverse {

	public static void main(String[] args) {

		int num = 0;
		int digit = 0;
		String rnum = "";

		Scanner inp = new Scanner(System.in);
		System.out.print("Please enter an integer number: ");
		num = inp.nextInt();

		while (num != 0) {
			digit = num % 10;
			rnum = rnum + digit;

			num = num / 10;
		}
		System.out.print("Reverse number: " + rnum);

	}
}