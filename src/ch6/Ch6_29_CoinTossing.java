package ch6;

import java.security.SecureRandom;
import java.util.Scanner;

public class Ch6_29_CoinTossing {
	private enum Coin {
		HEADS, TAILS
	}

	private static final SecureRandom random = new SecureRandom();

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int headSide = 0;
		int tailsSide = 0;

		System.out.print("How many times would you like to flip the coin? Enter 0 to end: ");

		int flipNumber;

		while ((flipNumber = input.nextInt()) != 0) {
			for (int x = 0; x < flipNumber; x++) {
				switch (flip()) {
				case HEADS:
					headSide++;
					break;
				case TAILS:
					tailsSide++;
					break;
				}
			}
			System.out.printf("Heads: %d Tails: %d\n", headSide, tailsSide);
			System.out.println("Your number of flips is " + (headSide + tailsSide));
		}
	}

	private static Coin flip() {
		if (random.nextBoolean()) {
			return Coin.HEADS;
		} else {
			return Coin.TAILS;
		}
	}

	// private static Coin flip() {
	// int r = random.nextInt(2);
	// if (r == 0) {
	// return Coin.HEADS;
	// } else {
	// return Coin.TAILS;
	// }
	// }
}