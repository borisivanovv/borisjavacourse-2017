//16.13 (Duplicate Elimination) Write a program that reads in a series of first names and eliminates
//duplicates by storing them in a Set. Allow the user to search for a first name.

package ch16ex13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Ch16_13_DupElim {

	public static void main(String[] args) {

		Scanner inp = new Scanner(System.in);

		System.out.print("Enter first names in a row separating them with space: ");
		String names = inp.nextLine();
		String[] arrayNames = names.split(" ");

		Set<String> set = new HashSet<String>(Arrays.asList(arrayNames));

		System.out.print("Unique first names: ");
		for (String value : set) {
			System.out.printf("%s ", value);
		}

		List<String> uniqueNames = new ArrayList<>(set);

		System.out.print("\nEnter the name you are looking for: ");
		String searchName = inp.nextLine();

		displaySearchResult(uniqueNames, searchName);

	}

	public static void displaySearchResult(List<String> list, String key) {
		int result = 0;
		result = Collections.binarySearch(list, key);

		if (result >= 0) {
			System.out.println("The name is found");
		} else {
			System.out.println("The name is not found");
		}

	}
}
