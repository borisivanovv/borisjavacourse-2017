package ch3ex14;

public class DateTest {

	public static void main(String[] args) {
		
		Date thedate = new Date(1,-3,2012);

		int day = thedate.getDay();
		int month = thedate.getMonth();
		int year = thedate.getYear();
		
		if (day == 0 || month == 0 || year == 0){
			System.out.println("Invalid date");			
		}
		else{
		System.out.printf("The date: %d/%d/%d",thedate.getMonth(), thedate.getDay(),thedate.getYear());
		}
	}

}
