package ch3ex14;

public class Date {

	private int year;
	private int month;
	private int day;

	public Date(int month, int day, int year) {
		setDay(day);
		setMonth(month);
		setYear(year);
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (year > 0) {
			this.year = year;
		}
		// else{
		// System.out.println("Invalid year");
		// }
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month > 0 && month < 13) {
			this.month = month;
		}
		// else{
		// System.out.println("Invalid month");
		// }
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day > 0 & day < 32) {
			this.day = day;
		}
		// else{
		// System.out.println("Invalid day");
		// }
	}

}
