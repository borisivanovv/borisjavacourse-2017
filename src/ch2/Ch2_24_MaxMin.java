//		2.24 (Largest and Smallest Integers) Write an application that reads five integers 
//and determines and prints the largest and smallest integers in the group. 
//Use only the programming techniques you learned in this chapter.

package ch2;

import java.util.Scanner;

public class Ch2_24_MaxMin {

	public static void main(String[] args) {
		
		Scanner inp = new Scanner(System.in);
		int num1 = 0, num2 = 0, num3 = 0, num4 = 0, num5 = 0, min =0, max = 0;
		
		System.out.print("Enter 5 integers: "); // prompt
		num1 = inp.nextInt();
		num2 = inp.nextInt();
		num3 = inp.nextInt();
		num4 = inp.nextInt();
		num5 = inp.nextInt();	
		
		min = num1;
		max = num2;
		
		if (min > num2){
			min = num2;
			max = num1;
		}
		
//		checking min
		if (min > num3){
			min = num3;
		} 
		if (min > num4){ 
			min = num4;
		} 
		if (min > num5){
			min = num5;
		}
		
//		checking max
		if (max < num3){
			max = num3;
		} 
		if (max < num4){ 
			max = num4;
		} 
		if (max < num5){
			max = num5;
		}
			
		System.out.println("The smallest number is "+min);
		System.out.println("The biggest number is "+max);
	}
}
