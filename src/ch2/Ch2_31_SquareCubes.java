//2.31 (Table of Squares and Cubes) Using only the programming techniques you learned in this
//chapter, write an application that calculates the squares and cubes of the numbers from 0 to 10 and
//prints the resulting values in table format, as shown below. 

package ch2;

public class Ch2_31_SquareCubes {

	public static void main(String[] args) {

		int num0 = 0;
		int num1 = 1;
		int num2 = 2;
		int num3 = 3;
		int num4 = 4;
		int num5 = 5;
		int num6 = 6;
		int num7 = 7;
		int num8 = 8;
		int num9 = 9;
		int num10 = 10;
		
		System.out.printf("%-8s%-8s%-8s\n","Number","Square","Cube");
		System.out.printf("%-8d%-8d%-8d\n",num0,num0*num0,num0*num0*num0);
		System.out.printf("%-8d%-8d%-8d\n",num1,num1*num1,num1*num1*num1);
		System.out.printf("%-8d%-8d%-8d\n",num2,num2*num2,num2*num2*num2);
		System.out.printf("%-8d%-8d%-8d\n",num3,num3*num3,num3*num3*num3);
		System.out.printf("%-8d%-8d%-8d\n",num4,num4*num4,num4*num4*num4);
		System.out.printf("%-8d%-8d%-8d\n",num5,num5*num5,num5*num5*num5);
		System.out.printf("%-8d%-8d%-8d\n",num6,num6*num6,num6*num6*num6);
		System.out.printf("%-8d%-8d%-8d\n",num7,num7*num7,num7*num7*num7);
		System.out.printf("%-8d%-8d%-8d\n",num8,num8*num8,num8*num8*num8);
		System.out.printf("%-8d%-8d%-8d\n",num9,num9*num9,num9*num9*num9);
		System.out.printf("%-8d%-8d%-8d",num10,num10*num10,num10*num10*num10);		
		
	}

}
