package ch2;

import java.util.Scanner;

public class Ch2_15_Arifmetic {

	public static void main(String[] args) {
		
		// create a Scanner to obtain input from the command window
		Scanner input = new Scanner(System.in);
		int number1; // first number
		int number2; // second number
		int sum; // sum of number1 and number2
		int product;
		int division;
		int difference;

		System.out.print("Enter first integer: "); // prompt
		number1 = input.nextInt();

		System.out.print("Enter second integer: "); // prompt
		number2 = input.nextInt(); // read second number from user
		
		sum = number1 + number2; // add numbers, then store total in sum
		product = number1 * number2;
		division = number1 / number2;
		difference = number1 - number2;
		
		System.out.printf("Sum is %d%n", sum); // display sum
		System.out.printf("Product is %d%n", product); // display product
		System.out.printf("Division is %d%n", division); // display division (quotinent)
		System.out.printf("Difference is %d%n", difference); // display difference
		

	}

}
