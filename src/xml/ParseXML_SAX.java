package xml;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseXML_SAX {

	public static void main(String args[]) {

		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {

				boolean bstudent = false;
				boolean bfname = false;
				boolean blname = false;
				boolean bbyear = false;
				boolean bamark = false;
				boolean baddress = false;
				boolean beprogram = false;
				String no = null;
				String domain;

				public void startElement(String uri, String localName, String qName, Attributes attributes)
						throws SAXException {

					System.out.println("Start Element :" + qName);

					if (qName.equalsIgnoreCase("student")) {
						bstudent = true;
						no = attributes.getValue("no");
					}

					if (qName.equalsIgnoreCase("FIRSTNAME")) {
						bfname = true;
					}

					if (qName.equalsIgnoreCase("LASTNAME")) {
						blname = true;
					}

					if (qName.equalsIgnoreCase("BIRTHYEAR")) {
						bbyear = true;
					}

					if (qName.equalsIgnoreCase("AVERAGEMARK")) {
						bamark = true;
					}

					if (qName.equalsIgnoreCase("ADDRESS")) {
						baddress = true;
					}

					if (qName.equalsIgnoreCase("EDUCATIONPROGRAM")) {
						beprogram = true;
						domain = attributes.getValue("domain");
					}

				}

				public void endElement(String uri, String localName, String qName) throws SAXException {

					System.out.println("End Element :" + qName);

				}

				public void characters(char ch[], int start, int length) throws SAXException {

					if (bstudent) {
						System.out.println("Student : " + no + new String(ch, start, length));
						bstudent = false;
					}

					if (bfname) {
						System.out.println("First Name : " + new String(ch, start, length));
						bfname = false;
					}

					if (blname) {
						System.out.println("Last Name : " + new String(ch, start, length));
						blname = false;
					}

					if (bbyear) {
						System.out.println("Birth Year : " + new String(ch, start, length));
						bbyear = false;
					}

					if (bamark) {
						System.out.println("Mark : " + new String(ch, start, length));
						bamark = false;
					}
					if (baddress) {
						System.out.println("Address : " + new String(ch, start, length));
						baddress = false;
					}

					if (beprogram) {
						// System.out.println(domain);
						System.out.println("Education Program : " + domain + "/" + new String(ch, start, length));
						beprogram = false;
					}

				}

			};

			saxParser.parse("Students.xml", handler);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}