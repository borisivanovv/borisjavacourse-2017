package xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ParseXML_Dom {

	public static void main(String argv[]) {

		try {

			File fXmlFile = new File("Students.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("student");

			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				System.out.println("\nCurrent Element :" + nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					System.out.println("Student Id : " + eElement.getAttribute("no"));
					System.out.println(
							"First Name : " + eElement.getElementsByTagName("firstName").item(0).getTextContent());
					System.out.println(
							"Last Name : " + eElement.getElementsByTagName("lastName").item(0).getTextContent());
					System.out.println(
							"Birth Year : " + eElement.getElementsByTagName("birthYear").item(0).getTextContent());
					System.out.println(
							"Average Mark : " + eElement.getElementsByTagName("averageMark").item(0).getTextContent());
					System.out
							.println("Address : " + eElement.getElementsByTagName("address").item(0).getTextContent());
					// System.out.println("Education Program : "
					// +
					// eElement.getElementsByTagName("educationProgram").item(0).getTextContent());
					System.out.println("Education Program : "
							+ eElement.getElementsByTagName("educationProgram").item(0).getAttributes().item(0)
									.getTextContent()
							+ "/" + eElement.getElementsByTagName("educationProgram").item(0).getTextContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}