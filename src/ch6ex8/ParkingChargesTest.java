package ch6ex8;

import java.util.Scanner;

public class ParkingChargesTest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double hours;
		double total = 0;
		double singleCharge;

		System.out.print("Enter the number of hours parked or -1 to quit: ");
		hours = sc.nextDouble();

		ParkingCharges calcualtedCharges = new ParkingCharges();

		while (hours != -1) {
			singleCharge = calcualtedCharges.calculateCharges(hours);
			total = total + singleCharge;
			System.out.println("Employee charge is: " + singleCharge);
			System.out.print("Total is: " + total);

			System.out.print("\nEnter the number of hours parked or -1 to quit: ");
			hours = sc.nextDouble();
		}

		System.out.println("Total charges of all employees: " + total);

	}

}
