package ch6ex8;

public class ParkingCharges {

	private final double upToThreeCharges = 2.0;
	private final double extraChargerPerHour = 0.5;
	private final double maxCharge = 10.0;

	public double calculateCharges(double hours) {
		double charges = 0;
		if (hours <= 3.0) {
			charges = upToThreeCharges;
		} else {
			charges = Math.ceil(hours - 3.0) * extraChargerPerHour + upToThreeCharges;
			if (charges > 10.0) {
				charges = maxCharge;
			}
		}
		return charges;
	}

}
