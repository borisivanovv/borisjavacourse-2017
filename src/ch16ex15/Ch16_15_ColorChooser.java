package ch16ex15;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

//16.15 (Color Chooser) Use a HashMap to create a reusable class for choosing one of the 13 predefined colors in class Color.
//The names of the colors should be used as keys, and the predefined
//Color objects should be used as values. Place this class in a package that can be imported into any
//Java program. Use your new class in an application that allows the user to select a color and draw a
//shape in that color

public class Ch16_15_ColorChooser {

	public static void main(String[] args) {

		Map<String, Color> colorMap = new HashMap<>();

		create(colorMap);
		display(colorMap);

	}

	private static void display(Map<String, Color> colorMap) {

		Set<String> keys = colorMap.keySet();
		TreeSet<String> sortedKeys = new TreeSet(keys);

		System.out.printf("%nMap contains:%nKey\t\tValue%n");

		// generate output for each key in map
		for (String key : sortedKeys) {
			System.out.printf("%-16s%10s%n", key, colorMap.get(key));
		}

	}

	private static void create(Map<String, Color> colorMap) {
		colorMap.put("black", Color.black);
		colorMap.put("red", Color.red);
		colorMap.put("blue", Color.blue);
		colorMap.put("white", Color.white);
		colorMap.put("green", Color.green);

	}

}
