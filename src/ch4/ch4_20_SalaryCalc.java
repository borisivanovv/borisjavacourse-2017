package ch4;

import java.util.Scanner;

public class ch4_20_SalaryCalc {
	public static void main(String[] args) {

		double hours;
		double rate;
		double grossPay;
		String name;

		Scanner sc = new Scanner(System.in);

		for (int i = 1; i <= 3; i++) {

			System.out.print("Enter the employee " + i + " name: ");
			name = sc.nextLine();

			System.out.print("Enter the employee " + i + " rate: ");
			rate = sc.nextDouble();

			System.out.print("Enter the employee " + i + " hours worked: ");
			hours = sc.nextDouble();

			if (hours > 40) {
				grossPay = rate * (1.5 * hours - 20);
			} else {
				grossPay = rate * hours;
			}

			System.out.printf("\n%s's Gross Pay is: %.2f\n\n", name, grossPay);

		}

	}
}
