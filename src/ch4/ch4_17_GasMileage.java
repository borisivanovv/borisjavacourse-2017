package ch4;

import java.util.Scanner;

public class ch4_17_GasMileage {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int gas;
		int km;
		int tripcounter = 0;
		int totalkm = 0;
		int totalgas = 0;

		System.out.print("Enter trip's distance in kms or -1 to quit: ");
		km = input.nextInt();

		System.out.print("Enter trip's gas used in litres or -1 to quit: ");
		gas = input.nextInt();

		while (km != -1 && gas != -1) {

			totalgas = totalgas + gas;
			totalkm = totalkm + km;
			tripcounter++;

			System.out.printf("\nTrip#: %d", tripcounter);
			System.out.printf("\nTrip km per liter: %.2f", (double) (km) / gas);
			System.out.printf("\nAverage km per liter: %.2f", 1.0 * totalkm / totalgas);

			System.out.print("\n\nEnter trip's distance in kms or -1 to quit: ");
			km = input.nextInt();

			System.out.print("Enter trip's gas used in litres or -1 to quit: ");
			gas = input.nextInt();
		}

		if (tripcounter == 0) {
			System.out.println("\nNot enough or no data was entered");
		}

	}

}
