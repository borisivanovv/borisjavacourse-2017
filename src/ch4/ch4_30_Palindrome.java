package ch4;

import java.util.Scanner;

public class ch4_30_Palindrome {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int number, char1, char2, char3, char4, char5;

		System.out.print("Enter the five digits positive integer number: ");
		number = input.nextInt();

		while (number < 10000 | number > 99999) {
			System.out.print("Invalid number. Enter the five digits positive integer number:");
			number = input.nextInt();
		}
		char1 = number % 10;
		char2 = number / 10 % 10;
		char3 = number / 100 % 10;
		char4 = number / 1000 % 10;
		char5 = number / 10000 % 10;
		if (char1 == char5 & char2 == char4) {
			System.out.println("The " + number + " is a palindrome");
		} else {
			System.out.println("The " + number + " is not a palindrome");
		}

	}

}
