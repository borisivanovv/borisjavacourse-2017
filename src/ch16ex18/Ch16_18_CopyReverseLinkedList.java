package ch16ex18;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

//16.18 (Copying and Reversing LinkedLists) Write a program that creates a LinkedList object of
//10 characters, then creates a second LinkedList object containing a copy of the first list, but in reverse order.

public class Ch16_18_CopyReverseLinkedList {

	public static void main(String[] args) {

		String[] colors = { "black", "yellow", "green", "blue", "violet", "silver" };

		List<String> list1 = new LinkedList<>();

		List<String> list2 = new LinkedList<>();

		for (String color : colors) {
			list1.add(color);
		}

		// Collections.sort(list1);
		System.out.printf("Original List:%n");

		for (String color : list1) {
			System.out.printf("%s ", color);
		}

		// printReversedList(list1);

		list2 = ReverseList(list1);

		for (String color : list2) {
			System.out.printf("%s ", color);
		}

	}

	// private static void printReversedList(List<String> list) {
	//
	// System.out.printf("%nReversed List:%n");
	// ListIterator<String> iterator = list.listIterator(list.size());
	// // print list in reverse order
	// while (iterator.hasPrevious()) {
	// System.out.printf("%s ", iterator.previous());
	// }
	// }

	private static List<String> ReverseList(List<String> list) {

		List<String> list2 = new LinkedList<>();

		System.out.printf("%nReversed List:%n");
		ListIterator<String> iterator = list.listIterator(list.size());

		// print list in reverse order
		while (iterator.hasPrevious()) {
			list2.add(iterator.previous());
		}

		return list2;
	}

}
