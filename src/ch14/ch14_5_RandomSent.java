//14.5 (Random Sentences) Write an application that uses random-number generation to create
//sentences. Use four arrays of strings called article, noun, verb and preposition. Create a sentence
//by selecting a word at random from each array in the following order: article, noun, verb, preposition, article and noun. 
//As each word is picked, concatenate it to the previous words in the sentence.
//The words should be separated by spaces. When the final sentence is output, it should start with a
//capital letter and end with a period. The application should generate and display 20 sentences.
//The article array should contain the articles "the", "a", "one", "some" and "any"; the noun
//array should contain the nouns "boy", "girl", "dog", "town" and "car"; the verb array should contain
//the verbs "drove", "jumped", "ran", "walked" and "skipped"; the preposition array should
//contain the prepositions "to", "from", "over", "under" and "on".

package ch14;

import java.security.SecureRandom;

public class ch14_5_RandomSent {

	public static void main(String[] args) {

		SecureRandom r = new SecureRandom();

		String[] articles = { "the", "a", "one", "some", "any" };
		String[] nouns = { "boy", "girl", "town", "car", "dog" };
		String[] verbs = { "drove", "jumped", "ran", "walked", "skipped" };
		String[] preps = { "to", "from", "over", "under", "on" };

		for (int i = 0; i < 20; i++) {

			if (i == 19) {

				String article, changedArticle;
				article = articles[1 + r.nextInt(4)];
				changedArticle = article.substring(0, 1).toUpperCase() + article.substring(1);

				System.out.println(changedArticle + " " + nouns[1 + r.nextInt(4)] + " " + verbs[1 + r.nextInt(4)] + " "
						+ preps[1 + r.nextInt(4)] + " " + articles[1 + r.nextInt(4)] + " " + nouns[1 + r.nextInt(4)]
						+ ".");

			} else {
				System.out.println(articles[1 + r.nextInt(4)] + " " + nouns[1 + r.nextInt(4)] + " "
						+ verbs[1 + r.nextInt(4)] + " " + preps[1 + r.nextInt(4)] + " " + articles[1 + r.nextInt(4)]
						+ " " + nouns[1 + r.nextInt(4)]);
			}

		}

	}

}
