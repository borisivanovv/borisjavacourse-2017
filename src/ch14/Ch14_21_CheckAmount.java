package ch14;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Ch14_21_CheckAmount {

	public static void main(String[] args) {

		Scanner inp = new Scanner(System.in);

		int checkNumInt;
		double checkNum;
		double cents;
		String checkWords = "";
		String centWords = "";

		Map<Integer, String> map = new TreeMap<Integer, String>();

		System.out.println("Enter the check amount within 0 - 1000 range");
		checkNum = inp.nextDouble();

		while (checkNum >= 1000 | checkNum <= 0) {
			System.out.println("Invalid amount ");
			System.out.println("Enter the check amount within 0 - 1000 range");
			checkNum = inp.nextDouble();
		}
		checkNumInt = (int) Math.floor(checkNum);
		create(map);

		cents = checkNum - checkNumInt;
		cents = (double) Math.floor(cents * 100) / 100;
		centWords = (int) (cents * 100) + "/100";
		// System.out.println(cents + " " + checkNumInt);
		// System.out.println(centWords);

		if (checkNumInt / 100 > 0) {
			checkWords = map.get(checkNumInt / 100) + " " + map.get(100) + " ";
			checkNumInt = checkNumInt - checkNumInt / 100 * 100;
			if (checkNumInt / 10 > 0 & checkNumInt > 20) {
				checkWords = checkWords + map.get((checkNumInt / 10) * 10) + " "
						+ map.get(checkNumInt - ((checkNumInt / 10) * 10));
			} else if (checkNumInt > 0) {
				checkWords = checkWords + map.get(checkNumInt);

			}
		} else if (checkNumInt / 10 > 0 & checkNumInt > 20) {
			checkWords = map.get((checkNumInt / 10) * 10) + " " + map.get(checkNumInt - ((checkNumInt / 10) * 10));
		} else {
			checkWords = map.get(checkNumInt);
		}

		if (cents == 0) {
			System.out.println(checkWords);
		} else {
			System.out.println(checkWords + " and " + centWords);
		}

	}

	private static void create(Map<Integer, String> map) {
		map.put(1, "one");
		map.put(2, "two");
		map.put(3, "three");
		map.put(4, "four");
		map.put(5, "five");
		map.put(6, "six");
		map.put(7, "seven");
		map.put(8, "eight");
		map.put(9, "nine");
		map.put(10, "ten");
		map.put(11, "eleven");
		map.put(12, "twelve");
		map.put(13, "thirteen");
		map.put(14, "fourteen");
		map.put(15, "fifteen");
		map.put(16, "sixteen");
		map.put(17, "seventeen");
		map.put(18, "eighteen");
		map.put(19, "nineteen");
		map.put(20, "twenty");
		map.put(30, "thirty");
		map.put(40, "forty");
		map.put(50, "fifty");
		map.put(60, "sixty");
		map.put(70, "seventy");
		map.put(80, "eighty");
		map.put(90, "ninety");
		map.put(100, "hundred");

	}

	// Set<Integer> keySet = map.keySet();
	// for (Integer key : keySet) {
	// System.out.println(key + " " + map.get(key));
	// }

}
