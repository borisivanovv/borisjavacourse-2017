package ch10ex13;

public class Rectangle extends TwoDimensionalShape {
	
	private double length2;
	
	public Rectangle(double length, double length2) {
		super(length);
		this.length2 = length2;
	}

	@Override
	public double getArea() {
		
		return super.getLength() * length2;

	}
	
	
	
	

}
