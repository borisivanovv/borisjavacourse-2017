package ch10ex13;

public class Sphere extends ThreeDimensionalShape {
	
	

	public Sphere(double length) {
		super(length);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getVolume() {
		return 4/3 * Math.PI * super.getLength() * super.getLength() * super.getLength();
	}

	@Override
	public double getArea() {
		
		return 4 * Math.PI * super.getLength() * super.getLength();

	}

}
