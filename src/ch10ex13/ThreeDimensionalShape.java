package ch10ex13;

public abstract class ThreeDimensionalShape extends Shape {

	public ThreeDimensionalShape(double length) {
		super(length);
	}
	
	public abstract double getVolume();

}
