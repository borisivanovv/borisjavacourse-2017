package ch10ex13;

public class Triangle extends TwoDimensionalShape {
	
	private double length2;
	private double length3;
	
	public Triangle(double length, double length2, double length3) {
		super(length);
		this.length2 = length2;
		this.length3 = length3;
	}

	@Override
	public double getArea() {
		
		double p = (super.getLength() + length2 + length3) / 2;
		
		return Math.sqrt(p * (p-super.getLength()) * (p-length2) * (p-length3));

	}	

}
