package ch10ex13;

public class GeomTest {

	public static void main(String[] args) {
		
		Shape[] shape = new Shape[5];
		
		shape[0] = new Rectangle(1,2);
		shape[1] = new Circle (2);
		shape[2] = new Triangle(2,3,4);
		shape[3] = new Sphere(2);
		shape[4] = new Cube(4);
		
		for (Shape s : shape) {
			System.out.printf("%s\n%s%.2f\n",s.toString(), "Area is ",s.getArea());
			
			if (s instanceof ThreeDimensionalShape) {
				ThreeDimensionalShape shp = (ThreeDimensionalShape) s;
				System.out.printf("%s%.2f\n","Volume is ", shp.getVolume());
			}
			System.out.println();
		}
		
	}

}
