package ch10ex13;

public abstract class TwoDimensionalShape extends Shape {

	public TwoDimensionalShape(double length) {
		super(length);
	}
}
