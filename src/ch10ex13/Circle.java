package ch10ex13;

public class Circle extends TwoDimensionalShape {
	
	public Circle(double length) {
		super(length);
	}

	public double getArea() {
		return Math.PI * super.getLength() * super.getLength();
	}
}
