package ch10ex13;

public class Cube extends ThreeDimensionalShape {

	public Cube(double length) {
		super(length);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getVolume() {
		return super.getLength() * super.getLength() * super.getLength() ;
	}

	@Override
	public double getArea() {
		return super.getLength() * super.getLength() * 6;

	}

}
