package ch8ex6;

//8.6 (Savings Account Class) Create class SavingsAccount. Use a static variable annualInterestRate
//to store the annual interest rate for all account holders. Each object of the class contains a
//private instance variable savingsBalance indicating the amount the saver currently has on deposit.
//Provide method calculateMonthlyInterest to calculate the monthly interest by multiplying the
//savingsBalance by annualInterestRate divided by 12—this interest should be added to savingsBalance.
//Provide a static method modifyInterestRate that sets the annualInterestRate to a new
//value. Write a program to test class SavingsAccount. Instantiate two savingsAccount objects,
//saver1 and saver2, with balances of $2000.00 and $3000.00, respectively. Set annualInterestRate
//to 4%, then calculate the monthly interest for each of 12 months and print the new balances for
//both savers. Next, set the annualInterestRate to 5%, calculate the next month’s interest and print
//the new balances for both savers.




public class SavingsAccountTest {

	public static void main(String[] args) {
		
		SavingsAccount saver1 = new SavingsAccount(2000.0);
		SavingsAccount saver2 = new SavingsAccount(3000.0);
		
		System.out.println("Annual Interest Rate is set to 4% and the balances are calculated with capitalization:");
		
		SavingsAccount.modifyInterestRate(0.04);
		
		System.out.printf("%-10s%-20s%-20s", "Month", "Monthly balance 1", "Monthly balance 2");
		
		for (int i = 0; i<12;i++) {
			
			double newBalance1 = saver1.getSavingsBalance() + saver1.calculateMonthlyInterest();
			double newBalance2 = saver2.getSavingsBalance() + saver2.calculateMonthlyInterest();
			saver1.setSavingsBalance(newBalance1);
			saver2.setSavingsBalance(newBalance2);
			System.out.printf("\n%-10d%-20.4f%-20.4f",i+1, newBalance1, newBalance2);
		}
		
		System.out.println("\n\nAnnual Interest Rate is set to 5% and the balances are calculated with capitalization:");
		SavingsAccount.modifyInterestRate(0.05);
		
		System.out.printf("%-10s%-20.4f%-20.4f","13", saver1.getSavingsBalance() + saver1.calculateMonthlyInterest(), 
				saver2.getSavingsBalance() + saver2.calculateMonthlyInterest());
//		System.out.println(saver1.getSavingsBalance() + saver1.calculateMonthlyInterest());
		

	}

}
