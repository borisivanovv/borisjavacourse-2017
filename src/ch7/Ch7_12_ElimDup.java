package ch7;

import java.util.Scanner;

public class Ch7_12_ElimDup {

	public static void main(String[] args) {
	      Scanner input = new Scanner( System.in );
	      
	      int[] array = new int[5];
	      int count = 0;
	      
	      while (count < array.length) {
	    	  
	    	  System.out.print("Enter the number: ");
	    	  int num = input.nextInt();
	    	  
	    	  if (num >= 10 && num <= 100) {
	    		  boolean numExists = false;
	    		  
	    		  for (int i = 0; i < count; i++) {
	    			  if (num == array[i]) {
	    				  numExists = true;  
	    			  }
	    		  }
	    		if (!numExists) {
	    		array[count] = num;
	    		count++;
	    		}
	    		else {
	    		 System.out.printf("%d already exists\n", num);
	    		}	    		  

	    	  }
	    	  else {
	    		  System.out.println("number must be between 10 and 100");
	    	  }
	    	  
	    	  for (int i = 0; i< count;i++) {
	    		  System.out.printf("%d ", array[i]);
	    	  }
	    	  
	      System.out.println();

	    	  
	      }
	}
}
