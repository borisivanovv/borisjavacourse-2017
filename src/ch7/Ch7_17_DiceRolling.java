//(Dice Rolling) Write an application to simulate the rolling of two dice. The application
//should use an object of class Random once to roll the first die and again to roll the second die. The
//sum of the two values should then be calculated. Each die can show an integer value from 1 to 6, so
//the sum of the values will vary from 2 to 12, with 7 being the most frequent sum, and 2 and 12 the
//least frequent. Figure 7.28 shows the 36 possible combinations of the two dice. Your application
//should roll the dice 36,000,000 times. Use a one-dimensional array to tally the number of times
//each possible sum appears. Display the results in tabular format.


package ch7;

import java.security.SecureRandom;

public class Ch7_17_DiceRolling {

	   public static void main(String[] args)
	   {
	      SecureRandom randomNumbers = new SecureRandom();
	      int[] frequency = new int[13]; // array of frequency counters
	      int face1 = 0;
	      int face2 = 0;

	      // roll die 6,000,000 times; use die value as frequency index
	      for (int roll = 1; roll <= 36000000; roll++) {
	    	  
	    	  face1 = 1 + randomNumbers.nextInt(6);
	    	  face2 = 1 + randomNumbers.nextInt(6);

	    	  ++frequency[face1+face2]; 	    	  
	      }

	      System.out.printf("%s%12s%n", "Face1 + Face2", "Frequency");
	   
	      // output each array element's value
	      for (int sum = 2; sum < frequency.length; sum++)
	         System.out.printf("%13d%12d%n", sum, frequency[sum]);
	   } 

}
