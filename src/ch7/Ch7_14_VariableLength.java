package ch7;

public class Ch7_14_VariableLength 
{
   public static int product(int... numbers)
   {
      int product = 1; 

      for (int d : numbers)
    	  product *= d;

      return product;
   } 

   public static void main(String[] args) 
   {
      int d1 = 10;
      int d2 = 20;
      int d3 = 30;
      int d4 = 40;

      System.out.printf("d1 = %d%nd2 = %d%nd3 = %d%nd4 = %d%n%n",
         d1, d2, d3, d4);

      System.out.printf("Product of d1 and d2 is %d%n", 
    		  product(d1, d2)); 
      System.out.printf("Product of d1, d2 and d3 is %d%n", 
    		  product(d1, d2, d3));
      System.out.printf("Product of d1, d2, d3 and d4 is %d%n", 
    		  product(d1, d2, d3, d4));
   } 
} 