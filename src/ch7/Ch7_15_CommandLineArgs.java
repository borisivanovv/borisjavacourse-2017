//7.15 (Command-Line Arguments) Rewrite Fig. 7.2 so that the size of the array is specified by the
//first command-line argument. If no command-line argument is supplied, use 10 as the default size
//of the array.

package ch7;

// Fig. 7.2: InitArray.java
// Initializing the elements of an array to default values of zero.

public class Ch7_15_CommandLineArgs 
{
   public static void main(String[] args)
   {
	   int arrayLength = 0;
	   
	   if (args.length == 0) {
		   arrayLength = 10;
		   System.out.println("No array length provided. Default array length is 10: " + args.length);
	   }
	   else {
	   	   arrayLength = Integer.parseInt(args[0]);	
	   }
	   
      // declare variable array and initialize it with an array object  
      int[] array = new int[arrayLength]; // new creates the array object 

      System.out.printf("%s%8s%n", "Index", "Value"); // column headings
   
      // output each array element's value 
      for (int counter = 0; counter < array.length; counter++)
         System.out.printf("%5d%8d%n", counter, array[counter]);
	   
   } 
} // end class InitArray