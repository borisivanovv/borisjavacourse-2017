//7.16 (Using the Enhanced for Statement) Write an application that uses an enhanced for state-
//ment to sum the double values passed by the command-line arguments. [Hint: Use the static
//method parseDouble of class Double to convert a String to a double value.]


package ch7;

public class Ch7_16_EnhFor {

	public static void main(String[] args) {
	
		double sum = 0;
		
		for (String i : args) {			
			sum += Double.parseDouble(i);			
		}
		System.out.printf("Total of array elements: %.2f%n", sum);

	}

}