//8.4 (Rectangle Class) Create a class Rectangle with attributes length and width, each of which
//defaults to 1. Provide methods that calculate the rectangle�s perimeter and area. It has set and get
//methods for both length and width. The set methods should verify that length and width are each
//floating-point numbers larger than 0.0 and less than 20.0. Write a program to test class Rectangle.

package ch8ex4;

public class Rectangle {

	double length = 1.0;
	double width = 1.0;

	public Rectangle(double length, double width) {
		setLength(length);
		setWidth(width);
	}

	public Rectangle() {
		setLength(length);
		setWidth(width);
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		if ((length > 0.0) & (length < 20.0)) {
			this.length = length;
		} else {
			throw new IllegalArgumentException("Length must larger than 0.0 and less than 20.0");
		}
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		if ((width > 0.0) & (width < 20.0)) {
			this.width = width;
		} else {
			throw new IllegalArgumentException("Width must larger than 0.0 and less than 20.0");
		}
	}

	public double calcPerimeter() {
		return 2 * length + 2 * width;

	}

	public double calcArea() {
		return length * width;

	}

}
