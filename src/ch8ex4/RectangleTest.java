//8.4 (Rectangle Class) Create a class Rectangle with attributes length and width, each of which
//defaults to 1. Provide methods that calculate the rectangle�s perimeter and area. It has set and get
//methods for both length and width. The set methods should verify that length and width are each
//floating-point numbers larger than 0.0 and less than 20.0. Write a program to test class Rectangle.

package ch8ex4;

public class RectangleTest {

	public static void main(String[] args) {

		Rectangle r1 = new Rectangle(2.5, 15.3);
		Rectangle r2 = new Rectangle();

		System.out.println("Rectangle 1 is " + r1.getLength() + "x" + r1.getWidth());
		System.out.println("Rectangle 1 perimeter is " + r1.calcPerimeter());
		System.out.println("Area 1 is " + r1.calcArea());

		try {
			Rectangle r3 = new Rectangle(27, 22); // invalid values
		} catch (IllegalArgumentException e) {
			System.out.printf("%nException while initializing Rectangle 3: %s%n", e.getMessage());
		}

		System.out.println("\nRectangle 2 is " + r2.getLength() + "x" + r2.getWidth());
		System.out.println("Rectangle 2 perimeter is " + r2.calcPerimeter());
		System.out.println("Area 2 is " + r2.calcArea());

	}

}
