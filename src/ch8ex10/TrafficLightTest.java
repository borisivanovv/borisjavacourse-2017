//8.10 Write an enum type TrafficLight, whose constants (RED, GREEN, YELLOW) take one parameter—the
//duration of the light. Write a program to test the TrafficLight enum so that it displays the
//enum constants and their durations

package ch8ex10;

public class TrafficLightTest {

	public static void main(String[] args) {
		
		System.out.println("light\tDuration\n");
		
		for (TrafficLight light: TrafficLight.values()) {
			System.out.printf("%s\t%d\n",light, light.getDuration());
		}
		

	}

}
