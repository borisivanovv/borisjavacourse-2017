//8.10 Write an enum type TrafficLight, whose constants (RED, GREEN, YELLOW) take one parameter—the
//duration of the light. Write a program to test the TrafficLight enum so that it displays the
//enum constants and their durations

package ch8ex10;

public enum TrafficLight {
	
	RED(100),
	GREEN(60),
	YELLOW(5);
	
	private final int duration;
	
	TrafficLight (int durationSecs){
		duration = durationSecs;
	}
	
	public int getDuration() {
		return duration;
	}
	
}
	