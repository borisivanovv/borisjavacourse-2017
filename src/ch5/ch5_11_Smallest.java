package ch5;

import java.util.Scanner;

public class ch5_11_Smallest {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int count;
		int num = 0;
		// int i = 2;
		int min = 0;

		System.out.print("How many numbers would you like to compare: ");
		count = sc.nextInt();

		// with while

		// System.out.println("Please enter " + count + " numbers");
		// System.out.print("Number 1: ");
		// num = sc.nextInt();
		// min = num;
		//
		// while (i <= count) {
		// System.out.print("Number " + i + ": ");
		// num = sc.nextInt();
		// if (min > num) {
		// min = num;
		// }
		// i++;
		// }

		// with for

		System.out.println("Please enter " + count + " numbers");
		System.out.print("Number 1: ");
		num = sc.nextInt();
		min = num;

		for (int i = 1; i < count; i++) {
			System.out.print("Number " + (i + 1) + ": ");
			num = sc.nextInt();
			if (min > num) {
				min = num;
			}

		}

		System.out.println("The smallest number is " + min);

	}

}
