package ch5;

import java.util.Scanner;

public class ch5_16_BarChart {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;
		String chartOfAst = "";

		System.out.print("Enter 5 integer numbers within the 1-30 range: ");

		for (int i = 1; i <= 5; i++) {
			num = input.nextInt();
			while (num < 1 | num > 30) {
				System.out.println("Invalid number. Please re-enter: ");
				num = input.nextInt();
			}
			for (int j = 0; j < num; j++) {
				chartOfAst = chartOfAst + "*";
			}
			chartOfAst = chartOfAst + "\n";
		}
		System.out.println(chartOfAst);

	}
}
